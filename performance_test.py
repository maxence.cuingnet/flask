import time

def main():
    start_time = time.time()

    # Effectuer les opérations de test de performance ici
    # Par exemple, une boucle de calcul intensif
    result = 0
    for i in range(1000000):
        result += i

    end_time = time.time()

    # Calculer le temps écoulé
    elapsed_time = end_time - start_time
    print("Temps écoulé:", elapsed_time, "secondes")

if __name__ == "__main__":
    main()