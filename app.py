from flask import Flask, render_template
from prometheus_client import start_http_server, Summary, Counter
import random
import time

app = Flask(__name__)

# Prometheus metrics
REQUEST_COUNT = Counter('flask_request_count', 'Number of requests')
REQUEST_LATENCY = Summary('flask_request_latency_seconds', 'Request latency')

@app.route('/')
def index():
    REQUEST_COUNT.inc()
    with REQUEST_LATENCY.time():
        time.sleep(random.uniform(0.1, 0.3))  # Simulate some processing time
    return render_template('index.html')

if __name__ == '__main__':
    # Start Prometheus metrics server
    start_http_server(8000)
    app.run(host='0.0.0.0', port=5000)