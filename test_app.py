import unittest
from app import app

class FlaskAppTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_home_page(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code, 200)
        self.assertIn(b'Bienvenue sur le serveur Flask', result.data)

    def test_metrics_endpoint(self):
        result = self.app.get('/metrics')
        self.assertEqual(result.status_code, 200)

if __name__ == '__main__':
    unittest.main()