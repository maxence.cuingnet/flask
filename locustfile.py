from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(1, 5)
    host = "http://localhost:5000"  # Replace with the actual host

    @task
    def my_task(self):
        self.client.get("/")
